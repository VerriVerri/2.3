using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using static Unity.VisualScripting.Member;

public class PlayerFPSController : MonoBehaviour
{
    public GameObject camerasParent; // The parent object of the camera
    public float maxVerticalAngle; // The maximum vertical angle that the camera can rotate
    public float minVerticalAngle; // The minimum vertical angle that the camera can rotate
    public float smoothTime = 0.05f; // The smoothing time for the camera rotation

    float vCamRotationAngles; // The current vertical camera rotation angle
    float hPlayerRotation; // The current horizontal player rotation angle
    Vector3 CurrentVVelocity; // The current vertical velocity of the camera rotation


    public GameObject cam;
    public float walkspeed = 5f;
    public float hRotationSpeed = 100f;
    public float vRotationSpeed = 80f;
    public Rigidbody rb;
    public Vector3 Force;
    public Vector3 Forcedown;
    private bool hasJumped = false;


    float CurrentHVelocity;
    float targetCamEulers;
    Vector3 targetCamRotation;

    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        GameObject.Find("Capsule").gameObject.SetActive(true);
        rb = GetComponent<Rigidbody>();
    }
    void Update()
    {
        movement();
        jump();
        cameraRotation();
    }
    private void movement()
    {
        float hMovement = Input.GetAxisRaw("Horizontal");
        float vMovement = Input.GetAxisRaw("Vertical");
        Vector3 movementDirection = hMovement * Vector3.right + vMovement * Vector3.up;

        //rb.velocity = movementDirection * walkspeed * Time.deltaTime;

        float vCamRotation = Input.GetAxis("Mouse Y") * vRotationSpeed * Time.deltaTime;
        float hPlayerRotation = Input.GetAxis("Mouse X") * hRotationSpeed * Time.deltaTime;

        // Limit the camera's vertical rotation
        float currentCamRotationX = cam.transform.localEulerAngles.x;
        currentCamRotationX = Mathf.Clamp(currentCamRotationX - vCamRotation, -180f, 180f);


        cam.transform.localEulerAngles = new Vector3(currentCamRotationX, 0f, 0f);
        transform.Rotate(0f, hPlayerRotation, 0f);
 
    }


    private void jump()
    {
        bool isGrounded = Physics.Raycast(transform.position, Vector3.down, 0.5f);

        if (Input.GetButtonDown("Jump"))
        {
            if (isGrounded)
            {
                // Jump if the player is on the ground
                GetComponent<Rigidbody>().velocity = Vector3.zero;
                rb.AddForce(Force, ForceMode.Impulse);
            }
        }
    }
    private void cameraRotation()
    {
        // Get the mouse movement in the horizontal and vertical axes
        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = Input.GetAxis("Mouse Y");

        // Calculate the new camera rotation angles based on the mouse movement
        //float hCamRotation = mouseX * hRotationSpeed * Time.deltaTime;
        //float vCamRotation = mouseY * vRotationSpeed * Time.deltaTime;

        // Clamp the vertical camera rotation angle to the specified min and max values
        //vCamRotationAngles = Mathf.Clamp(vCamRotationAngles - vCamRotation, minVerticalAngle, maxVerticalAngle);

        // Calculate the target camera rotation based on the new camera rotation angles
        //targetCamRotation = new Vector3(vCamRotationAngles, hPlayerRotation, 0f);

        // Smoothly rotate the camera towards the target rotation
        //camerasParent.transform.localEulerAngles = Vector3.SmoothDamp(camerasParent.transform.localEulerAngles, targetCamRotation, ref CurrentVVelocity, smoothTime);
    }

}